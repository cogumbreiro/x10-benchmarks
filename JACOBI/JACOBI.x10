import x10.array.*;
import x10.util.concurrent.AtomicDouble;
import x10.util.OptionsParser;
import x10.util.Option;

/************************************************************
* program to solve a finite difference 
* discretization of Helmholtz equation :  
* (d2/dx2)u + (d2/dy2)u - alpha u = f 
* using Jacobi iterative method. 
*
* Modified: Sanjiv Shah,       Kuck and Associates, Inc. (KAI), 1998
* Author:   Joseph Robicheaux, Kuck and Associates, Inc. (KAI), 1998
* This X10 version by David Grove, IBM Research, was translated in September 2013
* from the C translation written by Chunhua Liao, University of Houston, Jan, 2005.
* 
* Input :  n - grid dimension in x direction 
*          m - grid dimension in y direction
*          alpha - Helmholtz constant (always greater than 0.0)
*          tol   - error tolerance for iterative solver
*          relax - Successive over relaxation parameter
*          mits  - Maximum iterations for iterative solver
*
* On output 
*       : u(n,m) - Dependent variable (solutions)
*       : f(n,m) - Right hand side function 
*************************************************************/

public class JACOBI {
    static val D_ADD = (x:double,y:double)=>x+y;
   
    static val MSIZE = 200;
    static val MITER = 100; // 5000

    static val relax = 1.0;
    static val alpha = 0.0543;

    val n:long;
    val m:long;
    val u:Array_2[double];
    val uold:Array_2[double];
    val f:Array_2[double];

    val P:long = PlaceGroup.WORLD.size;

    public static def main(args:Rail[String]) {
        val opts = new OptionsParser(args, [Option.HELP as Option], [
            Option("s", "size", "Size of the matrix (default " + MSIZE + ")"),
            Option("i", "miter", "Maximum iterations (default " + MITER + ")")
        ]);
        if (opts.wantsUsageOnly("")) return;
        val s=opts("-s", MSIZE);
        val n=s;
        val m=s;
        val tol=0.0000000001;
        val mits=opts("-i", MITER);

        val jb = new JACOBI(m, n);
        Console.OUT.println("Running using "+jb.P+" threads, (size="+s+", max_iter="+mits+")...");

        val start = System.nanoTime();
        jb.jacobi(tol, mits);
        val end = System.nanoTime();

        Console.OUT.println("------------------------");
        Console.OUT.println("Execution time = "+((end-start)/1E9));
        jb.errorCheck();
    }

    /** 
     * Initializes data assuming exact solution is u(x,y) = (1-x^2)*(1-y^2)
     */
    def this(n:long, m:long) {
        val dx = 2.0 / (n-1);
        val dy = 2.0 / (m-1);
        this.m = m;
        this.n = n;
        this.u = new Array_2[double](n,m);
        this.uold = new Array_2[double](n,m);
        this.f = new Array_2[double](n, m, (i:long,j:long) => {
            val xx = (-1.0 + dx * (i-1)) as int;
            val yy = (-1.0 + dy * (j-1)) as int;
            -1.0*alpha *(1.0-xx*xx)*(1.0-yy*yy) -2.0*(1.0-xx*xx)-2.0*(1.0-yy*yy)
        });
    }

   /******************************************************************
    * Subroutine HelmholtzJ
    * Solves poisson equation on rectangular grid assuming : 
    * (1) Uniform discretization in each direction, and 
    * (2) Dirichlect boundary conditions 
    * 
    * Jacobi method is used in this routine 
    *
    * Input : n,m   Number of grid points in the X/Y directions 
    *         dx,dy Grid spacing in the X/Y directions 
    *         alpha Helmholtz eqn. coefficient 
    *         omega Relaxation factor 
    *         f(n,m) Right hand side function 
    *         u(n,m) Dependent variable/Solution
    *         tol    Tolerance for iterative solver 
    *         maxit  Maximum number of iterations 
    *
    * Output : u(n,m) - Solution 
    *****************************************************************/
    def jacobi(tol:double, mits:long) {
        val omega=relax;
        val dx = 2.0 / (n-1);
        val dy = 2.0 / (m-1);

        // Initialize coefficients
        val ax = 1.0/(dx*dx); /* X-direction coef */
        val ay = 1.0/(dy*dy); /* Y-direction coef */
        val b  = -2.0/(dx*dx)-2.0/(dy*dy) - alpha; /* Central coeff */ 

        val is = new Rail[DenseIterationSpace_1](P, (i:long)=>BlockingUtils.partitionBlock(1,n-2,P, i));
        var maxBlockSize:Long = 0;
        for (blk in is) {
            maxBlockSize = Math.max(maxBlockSize, Util.countElems(blk));
        }
        val shmem = new GlobalRail[Double](new Rail[Double](n*m));
        val error = at (here.next()) CollectiveArray[Double](1);
        val ROOT = here.id;
        finish async {
            val c = Clock.make();
            for (plc in PlaceGroup.WORLD) at(plc) async clocked(c) {
                val myId = here.id;
                val block = is(myId);
                //Console.OUT.println(here + " block=" + block);
                var residual:Double = 10.0 * tol;
                var k:Long = 1;
                while (k <= mits && residual > tol) {
                    finish {
                        // copy my portion of new solution to old
                        // do it in parallel with the local error counting
                        Util.asyncCopy(u, block, shmem);
                        var my_error:double = 0.0;
                        for ([i] in block) {
                            for (j in 1..(m-2)) {
                                val resid = (ax*(uold(i-1, j) + uold(i+1, j)) +
                                             ay*(uold(i, j-1) + uold(i, j+1)) + 
                                             b * uold(i, j) - f(i, j))/b;  
                                u(i, j) = uold(i, j) - omega * resid;  
                                my_error += resid*resid;   
                             }
                        }
                        error.set(my_error);
                    } // wait for the array copy to finish
                    Clock.advanceAll();
                    finish {
                        // copy the shared version of 'u' to 'u_old'
                        // in parallel with the reduction
                        Util.asyncCopy(shmem, uold);
                        // we want to do a reduce all, so we need to sync
                        error.reduce(1, D_ADD, 0.0, ROOT);
                        Clock.advanceAll(); // wait for reduce to conclude
                        k += 1;
                        residual = Math.sqrt(error.getFrom(ROOT))/(n*m);
                        if (here.id == ROOT) {
                            //Console.OUT.println(error.getFrom(error.home));
                            if (k%500==0) { Console.OUT.println("Finished "+k+" iteration."); }
                        }
                    } // wait for the download to finish
                }
                if (here.id == ROOT) {
                    Console.OUT.println("Total Number of Iterations:"+k);
                    Console.OUT.println("Residual:"+residual);
                }
            }
            c.drop();
        }
    }


    /************************************************************
    * Checks error between numerical and exact solution 
    ************************************************************/ 
    def errorCheck() {
        val dx = 2.0 / (n-1);
        val dy = 2.0 / (m-1);
        var error:double = 0.0 ;

        for (i in 0..(n-1)) {
            for (j in 0..(m-1)) {
               val xx = -1.0 + dx * (i-1);
               val yy = -1.0 + dy * (j-1);
               val temp  = u(i,j) - (1.0-xx*xx)*(1.0-yy*yy);
               error = error + temp*temp; 
            }
        }
        error = Math.sqrt(error)/(n*m);
        Console.OUT.println("Solution Error :"+error);
    }
}
