import x10.array.*;
import x10.compiler.Inline;

class Util {
    @Inline
    public static def countElems(is:DenseIterationSpace_1) {
        return is.isEmpty() ? 0 : 1 + is.max - is.min;
    }
    /**
     * Copies from (a part of) the local array to the collective array.
     */
    public static def asyncCopy[T](src:Array_2[T], row:DenseIterationSpace_1, dst:GlobalRail[T]) {
        val blockSize = row.isEmpty() ? 0 : 1 + row.max - row.min;
        if (blockSize > 0) {
            val offset = src.offset(row.min, 0);
            val count = blockSize * src.numElems_2;
            Rail.asyncCopy(src.raw(), offset, dst, offset, count);
        }
    }
    
    /**
     * Copies from (a part of) the local array to the collective array.
     */
    public static def asyncCopy[T](src:Array_2[T], row:DenseIterationSpace_1, dst:CollectiveArray[T]) {
        val blockSize = row.isEmpty() ? 0 : 1 + row.max - row.min;
        if (blockSize > 0) {
            val offset = src.offset(row.min, 0);
            val count = blockSize * src.numElems_2;
            dst.asyncAllGather(src.raw(), offset, offset, count);
        }
    }
    
    /**
     * Copies from the collective array to the local array.
     */
    public static def asyncCopy[T](src:CollectiveArray[T], dst:Array_2[T]) {
        val idx = src.placeGroup.indexOf(here);
        val count = dst.numElems_1 * dst.numElems_2;
        CollectiveArray.asyncCopy(src, idx, 0, dst.raw(), 0, count);
    }
    
    /**
     * Copies from the collective array to the local array.
     */
    public static def asyncCopy[T](src:GlobalRail[T], dst:Array_2[T]) {
        val count = dst.numElems_1 * dst.numElems_2;
        Rail.asyncCopy(src, 0, dst.raw(), 0, count);
    }
    
    private static def printArray[T](arr:Array_2[T]) {
        for (p in arr.indices()) {
            Console.OUT.println(p + "=" + arr(p));
        }
    }
    
    public static def main(args:Rail[String]) {
        val P = PlaceGroup.WORLD.size;
        val n = 3;
        val m = 4;
        val is = new Rail[DenseIterationSpace_1](P, (i:long)=>BlockingUtils.partitionBlock(0,n-1,P, i));
        val buff = new CollectiveArray[Long](n * m);
        val col = 0..(m - 1);
        val c = Clock.make();
        for (plc in PlaceGroup.WORLD) at(plc) async clocked(c) {
            val arr = new Array_2[Long](n, m);
            val row = is(here.id);
            // initialize
            for ([i] in row) {
                for (j in col) {
                    arr(i,j) = (here.id + 1) * 100 + i * 10 + j;
                }
            }
            atomic {Console.OUT.println(here); printArray(arr);}
            finish asyncCopy(arr, row, buff);
            Clock.advanceAll();
            finish asyncCopy(buff, arr);
            
            if (here.id == 0) {
                Console.OUT.println("***********");
                printArray(arr);
            }
        }
        c.drop();
    } 
}
