import x10.array.*;
import x10.util.*;
import x10.compiler.Inline;

public struct CollectiveArray[T] {
    val placeGroup:PlaceGroup;
    val arr:GlobalRail[T];
    val blockSize:Long;
    val home:Place;
    val placeRange:LongRange;
    val blockRange:LongRange;
    
    public def this(blockSize:Long) {T haszero} {
        this(PlaceGroup.WORLD, blockSize);
    }
    
    public def this(placeGroup:PlaceGroup, blockSize:Long) {T haszero} {
        this(placeGroup, blockSize, new Rail[T](blockSize * placeGroup.size()));
    }
    
    private def this(placeGroup:PlaceGroup, blockSize:Long, raw:Rail[T]{self != null}) {
        this.placeGroup = placeGroup;
        this.blockSize = blockSize;
        this.arr = new GlobalRail[T](raw);
        this.home = here;
        this.placeRange = 0..(placeGroup.size() - 1);
        this.blockRange = 0..(blockSize - 1);
    }

    public def blockSize() = blockSize;
    
    @Inline
    public def set(v:T) {
        val idx = currentOffset(0);
        at (arr.home) arr(idx) = v;
    }
    
    @Inline
    public def getFrom(placeId:Long) {
        val idx = offset(placeId, 0);
        return at(arr.home) arr(idx);
    }
        
    @Inline
    public def get():T {
        return getFrom(currentIndex());
    }

    @Inline
    public def getFrom(root:Place):T {
        return getFrom(indexOf(root));
    }
    
    /**
     * Reduces values on all processes within a group. 
     * The result is available in the buffer of 'root' after sync.
     */
    @Inline
    public def reduce(op:(T,T)=>T, unit:T, root:Long) {
        reduce(blockSize, op, unit, root);
    }
    @Inline
    public def reduce(op:(T,T)=>T, root:Long) {T haszero} {
        reduce(op, Zero.get[T](), root);
    }
    /**
     * Reduces values on all processes within a group. 
     * The result is available in the buffer of 'root' after sync.
     */
    public def reduce(count:Long, op:(T,T)=>T, unit:T, root:Long) {
        assert count <= blockSize;
        if (here == arr.home) {
            doReduce(count, op, unit, root);
        }
    }
    
    private def doReduce(count:Long, op:(T,T)=>T, unit:T, root:Long) {here == arr.home} {
        val buff = arr();
        for (idx in 0..(count - 1)) {
            // reduce
            var v:T = unit;
            for (p in placeRange) {
                v = op(v, buff(offset(p, idx)));
            }
            buff(offset(root, idx)) = v;
        }
    }
    
    @Inline private def offset(placeId:Long, idx:Long) {
        return blockSize * placeId + idx;
    }
    
    @Inline private def currentOffset(idx:Long) {
        return offset(currentIndex(), idx);
    }
    
    @Inline private def currentIndex() {
        return placeGroup.indexOf(here);
    }
    @Inline private def indexOf(p:Place) {
        return placeGroup.indexOf(p);
    }

    public def asyncAllToAll(dst:Rail[T]{self != null}, dst_offset:Long, count:Long) {
        if ((count * placeGroup.size) > (dst.size - dst_offset)) {
            var msg:String = "count * placeGroup.size > (dst.dize - dst_offset)";
            msg += ", count=" + count;
            msg += ", placeGroup.size=" + placeGroup.size;
            msg += ", dst.size=" + dst.size;
            msg += ", dst_offset=" + dst_offset;
            throw new IllegalArgumentException(msg); 
        }
        val currOffset = count * currentIndex();
        for (id in placeRange) {
            Rail.asyncCopy(arr, offset(id, currOffset), dst, dst_offset + count * id, count);
        }
    }
    
    public def asyncAllGather(src:Rail[T]{self != null}, srcOffset:Long, dstOffset:Long, count:Long) {
        for (id in placeRange) {
            Rail.asyncCopy(src, srcOffset, arr, offset(id, dstOffset), count);
        }
    }
    
    @Inline public def asyncAllGather(src:Rail[T]{self != null}, count:Long) {
        asyncAllGather(src, 0, count * currentIndex(), count);
    }
    
    // The only way to move data in/out of the collective array.
    @Inline
    public static def asyncCopy[T](src:CollectiveArray[T], placeId:Long, srcOffset:Long, dst:Rail[T], dstOffset:Long, count:Long) {
        Rail.asyncCopy(src.arr, src.offset(placeId, srcOffset), dst, dstOffset, count);
    }
    @Inline
    public static def asyncCopy[T](src:CollectiveArray[T], placeId:Long, dst:Rail[T]) {
        asyncCopy(src, placeId, 0, dst, 0, src.blockSize);
    }
    @Inline
    public static def asyncCopy[T](src:Rail[T], srcOffset:Long, dst:CollectiveArray[T], placeId:Long, dstOffset:Long, count:Long) {
        Rail.asyncCopy(src, srcOffset, dst.arr, dst.offset(placeId, dstOffset), count);
    }
    @Inline
    public static def asyncCopy[T](src:Rail[T], dst:CollectiveArray[T], placeId:Long) {
        asyncCopy(src, 0, dst, placeId, 0, dst.blockSize);
    }
    public static def example1() {
	    val c = Clock.make();
	    val len = 8;
	    val blockLen = 8;
	    val cv = new CollectiveArray[Long](PlaceGroup.WORLD, len);
	    for (p in PlaceGroup.WORLD) at(p) async clocked(c) {
    	    val buff = new Rail[Long](blockLen);
    	    val buff_out = new Rail[Long](blockLen);
	        for (i in buff.range()) {
	            buff(i) = i + 10 * (here.id);
	        }
            if (here.id() == 0) Console.OUT.println(here + " = " + buff);
            finish asyncCopy(buff, cv, here.id);
            c.advance();
            finish cv.asyncAllToAll(buff_out, 0, 2);
	        if (here.id == 0) Console.OUT.println(here + " = " + buff_out);
	    }
	    c.drop();
    }
    public static def example2() {
	    val c = Clock.make();
	    val len = 8;
	    val blockLen = 8;
	    val cv = new CollectiveArray[Long](PlaceGroup.WORLD, len);
	    for (p in PlaceGroup.WORLD) at(p) async clocked(c) {
    	    val buff = new Rail[Long](blockLen);
    	    val buff_out = new Rail[Long](blockLen);
	        for (i in buff.range()) {
	            buff(i) = i + 10 * (here.id);
	        }
            if (here.id() == 0) Console.OUT.println(here + " = " + buff);
            finish cv.asyncAllGather(buff, 2);
            c.advance();
            finish asyncCopy(cv, here.id, buff_out);
	        if (here.id == 0) Console.OUT.println(here + " = " + buff_out);
	    }
	    c.drop();
    }
	public static def main(Rail[String]) {
	    finish async {
	        example1();
	    }
	/*
	    val c = Clock.make();
	    val len = 1;
	    val cv = new CollectiveArray[Long](PlaceGroup.WORLD, len);
	    //val buff = new Rail[Long](3);
	    val root = here;
	    for (p in PlaceGroup.WORLD) at(p) async clocked(c) {
    	    val buff = new Rail[Long](len);
    	    val buff_out = new Rail[Long](len);
	        for (i in buff.range()) {
	            buff(i) = i + 10 * (here.id);
	        }
            if (here.id() == 0) Console.OUT.println(here + " = " + buff);
	        /*
	        buff(0) = p.id();
	        buff(1) = 1;* /
	        //cv.allGather(buff, 3);
	        //cv.copyFrom(buff);
	        buff(0) = here.id() + 1;
	        //finish asyncCopy(buff, 0, cv, 0, here.id, 1);
	        cv.set(here.id() + 1);
	        c.advance();
	        if (here.id == 0) {
	            at (cv.home) {
	                Console.OUT.println(cv.arr());
	            }
	        }
	        cv.reduce(1, (x:long,y:long)=>x+y, 0, 0);
	        c.advance();
	        //cv.copyTo(buff_out);
	        //cv.allToAll(buff_out, 2);
	        //cv.allreduce((x:Long,y:Long)=>x+y);
	        //c.advance();
	        //if (here.id() == 0) Console.OUT.println(here + " = " + buff_out);
	        Console.OUT.println(here + " sum="+ cv.getFrom(root));
	    }
	    c.drop();
	    */
	}
}
