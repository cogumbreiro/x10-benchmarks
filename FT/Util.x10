public struct Útil {
    public static struct C(im:Double,re:Double) {
        def this(c:Complex) {
            property(c.im, c.re);
        }
        public def toString():String {
            return String.format("%.9f + %.9fi", [re as Any, im]);
        }
    }
    public static def toC(rail:Rail[Complex]):Rail[C] =
        new Rail[C](rail.size, (x:Long)=>C(rail(x))) + "";
        
    public static def toH(rail:Rail[Complex]) = hash(toC(rail));
    
    public static def hash(data:String) = String.format("%x", [data.hashCode() as Any]);
    
    public static def allExit() {
        Team.WORLD.barrier();
        System.killHere();
    }
    
    def allPrint(msg:Any) {
        Console.OUT.println(here + " " + msg);
        Team.WORLD.barrier();
    }

}
