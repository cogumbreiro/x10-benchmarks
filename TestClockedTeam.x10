import x10.util.*;

public class TestClockedTeam {
    public static def expected_allreduce(setupIn:(id:Long)=>Rail[Long], setupOut:PlaceLocalHandle[Rail[Long]], src_off:Long, dst_off:Long, count:Long) {
        for (plc in PlaceGroup.WORLD) at (plc) async {
            val in_buff = setupIn(here.id);
            val out_buff = setupOut();
            Team.WORLD.allreduce(in_buff, src_off, out_buff, dst_off, count, Team.ADD);
        }
    }
    
    public static def obtained_allreduce(setupIn:(id:Long)=>Rail[Long], setupOut:PlaceLocalHandle[Rail[Long]], src_off:Long, dst_off:Long, count:Long) {
        val buff = new CollectiveArray[Long](setupIn(0).size * PlaceGroup.WORLD.size);
        val c = Clock.make();
        for (plc in PlaceGroup.WORLD) at (plc) async clocked(c) {
            val team = new ClockedTeam(buff, c, here.id);
            val in_buff = setupIn(here.id);
            val out_buff = setupOut();
            team.allreduce(in_buff, src_off, out_buff, dst_off, count, ClockedTeam.L_ADD);
        }
        c.drop();
    }
    
    public static def expected_alltoall(setupIn:(id:Long)=>Rail[Long], setupOut:PlaceLocalHandle[Rail[Long]], src_off:Long, dst_off:Long, count:Long) {
        for (plc in PlaceGroup.WORLD) at (plc) async {
            val in_buff = setupIn(here.id);
            val out_buff = setupOut();
            Team.WORLD.alltoall(in_buff, src_off, out_buff, dst_off, count);
        }
    }
    
    public static def obtained_alltoall(setupIn:(id:Long)=>Rail[Long], setupOut:PlaceLocalHandle[Rail[Long]], src_off:Long, dst_off:Long, count:Long) {
        val buff = new CollectiveArray[Long](setupIn(0).size * PlaceGroup.WORLD.size);
        val c = Clock.make();
        for (plc in PlaceGroup.WORLD) at (plc) async clocked(c) {
            val team = new ClockedTeam(buff, c, here.id);
            val in_buff = setupIn(here.id);
            val out_buff = setupOut();
            team.alltoall(in_buff, src_off, out_buff, dst_off, count);
        }
        c.drop();
    }

    public static def equals[T](expected:Rail[T], obtained:Rail[T], size:Long):Boolean {
        for (x in 0..(size -1)) {
            if (expected(x) != obtained(x)) {
                return false;
            }
        }
        return true;
    }

    public static def equals[T](expected:Rail[T], obtained:Rail[T]):Boolean {
        if (expected.size != obtained.size) {
            return false;
        }
        for (x in expected.range()) {
            if (expected(x) != obtained(x)) {
                return false;
            }
        }
        return true;
    }

    public static def assert_equals_alltoall(size:Long, setupIn:(id:Long)=>Rail[Long], src_off:Long, dst_off:Long, count:Long) {
        val globalSetupOut1 = PlaceLocalHandle.makeFlat[Rail[Long]](PlaceGroup.WORLD, ()=> new Rail[Long](size));
        val globalSetupOut2 = PlaceLocalHandle.makeFlat[Rail[Long]](PlaceGroup.WORLD, ()=> new Rail[Long](size));
        finish expected_alltoall(setupIn, globalSetupOut1, src_off, dst_off, count);
        finish obtained_alltoall(setupIn, globalSetupOut2, src_off, dst_off, count);
        finish for (plc in PlaceGroup.WORLD) at (plc) async {
            val expected = globalSetupOut1();
            val obtained = globalSetupOut2();
            if (!equals(expected, obtained, PlaceGroup.WORLD.size * count)) {
                var msg:String = here + " =>Failed alltoall(";
                msg += "size=" + size + ", src_off="+ src_off;
                msg += ", dst_off=" + dst_off + ", count=" + count + ")\n";
                msg += "expected: " + expected + "\n"; 
                msg += "obtained: " + obtained + "\n";
                atomic Console.OUT.println(msg);
            }
        }
    }

    public static def assert_equals_allreduce(size:Long, setupIn:(id:Long)=>Rail[Long], src_off:Long, dst_off:Long, count:Long) {
        val globalSetupOut1 = PlaceLocalHandle.makeFlat[Rail[Long]](PlaceGroup.WORLD, ()=> new Rail[Long](size));
        val globalSetupOut2 = PlaceLocalHandle.makeFlat[Rail[Long]](PlaceGroup.WORLD, ()=> new Rail[Long](size));
        finish expected_allreduce(setupIn, globalSetupOut1, src_off, dst_off, count);
        finish obtained_allreduce(setupIn, globalSetupOut2, src_off, dst_off, count);
        finish for (plc in PlaceGroup.WORLD) at (plc) async {
            val expected = globalSetupOut1();
            val obtained = globalSetupOut2();
            if (!equals(expected, obtained, PlaceGroup.WORLD.size * count)) {
                var msg:String = here + " =>Failed alltoall(";
                msg += "size=" + size + ", src_off="+ src_off;
                msg += ", dst_off=" + dst_off + ", count=" + count + ")\n";
                msg += "expected: " + expected + "\n"; 
                msg += "obtained: " + obtained + "\n";
                atomic Console.OUT.println(msg);
            }
        }
    }

    public static def alltoall() {
        for (size in 9..100) {
            for (src_off in 0..0) { // if we change the src offset the Team API breaks
                for (dst_off in 0..0/*(size - 1)*/) {
                    for (count in 1..(size / PlaceGroup.WORLD.size)) {
                        val setupIn = (id:Long) => new Rail[Long](size, (x:Long) => size * (id + 1) + x);
                        //val count = size / 2;
                        assert_equals_alltoall(size, setupIn, src_off, dst_off, count);
                        Console.OUT.print(".");
                    }
                }
            }
        }
        Console.OUT.println("Done!");
    }

    public static def allreduce() {
        for (size in 50..100) {
            for (src_off in 0..0) { // if we change the src offset the Team API breaks
                for (dst_off in 0..0/*(size - 1)*/) {
                    for (count in 1..(size / PlaceGroup.WORLD.size)) {
                        val setupIn = (id:Long) => new Rail[Long](size, (x:Long) => size * (id + 1) + x);
                        //val count = size / 2;
                        assert_equals_allreduce(size, setupIn, src_off, dst_off, count);
                        Console.OUT.print(".");
                    }
                }
            }
        }
        Console.OUT.println("Done!");
    }
    
    final static def randomComplex(r:Random)=Complex(r.nextDouble()-0.5, r.nextDouble()-0.5);
    public static def main(Rail[String]) {
        val r = new Random(0);
        alltoall();
        allreduce();
    }
}
