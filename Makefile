ARMUSC_JAR ?= ../armus-x10/target/armusc-x10.jar
JAVA ?= java
ARMUSC = $(JAVA) -jar $(ARMUSC_JAR)

all: KMEANS.jar FT.jar SSCA2.jar STREAM.jar JACOBI.jar

checked: KMEANS-C.jar FT-C.jar SSCA2-C.jar STREAM-C.jar JACOBI-C.jar

%-C.jar: %.jar
	$(ARMUSC) $< $@

%.jar: %
	(cd $<; make)

clean:
	rm -f *.jar *.so
