public struct ClockedTeam[T] (buff:CollectiveArray[T], clock:Clock, placeId:Long) {
    public static val D_ADD = (x:Double,y:Double)=>x+y;
    public static val F_ADD = (x:Float,y:Float)=>x+y;
    public static val I_ADD = (x:Int,y:Int)=>x+y;
    public static val L_ADD = (x:Long,y:Long)=>x+y;
    
    private static val ROOT = 0;
    
    /** Blocks until all members have received their part of each other member's array.
     * Each member receives a contiguous and distinct portion of the src array.
     * src should be structured so that the portions are sorted in ascending
     * order, e.g., the first member gets the portion at offset src_off of sbuf, and the
     * last member gets the last portion.
     *
     * @param src The data that will be sent (will only be used by the root
     * member)
     *
     * @param src_off The offset into src at which to start reading
     *
     * @param dst The rail into which the data will be received for this member
     *
     * @param dst_off The offset into dst at which to start writing
     *
     * @param count The number of elements being transferred
     */
    public def alltoall(src:Rail[T], src_off:Long, dst:Rail[T]{self != null}, dst_off:Long, count:Long) : void {
        finish CollectiveArray.asyncCopy(src, src_off, buff, placeId, 0, src.size - src_off);
        clock.advance();
        finish buff.asyncAllToAll(dst, dst_off, count);
        clock.advance();
    }    
    
    /** Blocks until all members have received the computed result.  Note that not all values of T are valid.
     * The dst array is populated for all members with the result of the operation applied pointwise to all given src arrays.
     *
     * @param src The data that will be sent to all members
     *
     * @param src_off The offset into src at which to start reading
     *
     * @param dst The rail into which the data will be received for this member
     *
     * @param dst_off The offset into dst at which to start writing
     *
     * @param count The number of elements being transferred
     *
     * @param op The operation to perform
     */
    public def allreduce (src:Rail[T], src_off:Long, dst:Rail[T], dst_off:Long, count:Long, op:(x:T,y:T)=>T) {T haszero} : void {
        finish CollectiveArray.asyncCopy(src, src_off, buff, placeId, 0, count);
        clock.advance();
        buff.reduce(count, op, Zero.get[T](), ROOT);
        clock.advance();
        finish CollectiveArray.asyncCopy(buff, ROOT, 0, dst, dst_off, count);
    }
    
    /** Blocks until all team members have reached the barrier. */
    public def barrier() {
        clock.advance();
    }
}
